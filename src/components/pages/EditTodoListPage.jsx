import React, { Component } from 'react';
import EditTodoList from '../EditTodoList';

class EditTodoListPage extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <EditTodoList />
      </React.Fragment>
    );
  }
}

export default EditTodoListPage;
