import React, { Component } from 'react';
import AddTodoItem from '../AddTodoItem';

class AddTodoItemPage extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <AddTodoItem />
      </React.Fragment>
    );
  }
}

export default AddTodoItemPage;
