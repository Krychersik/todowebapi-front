import React, { Component } from 'react';

class AboutPage extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <h1>About</h1>
        <p>This is the TodoList app v1.0.0.</p>
      </React.Fragment>
    );
  }
}

export default AboutPage;
