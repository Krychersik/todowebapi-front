import React, { Component } from 'react';
import TodoList from '../TodoList';

class TodoListPage extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <TodoList />
      </React.Fragment>
    );
  }
}

export default TodoListPage;
