import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class SuccessAlert extends Component {
  getSuccessAlert() {
    const hasSuccess = this.props.success.length > 0;

    if (hasSuccess) {
      return (
        <div className="alert alert-success m-2">
          {this.props.success.map((success, index) => (
            <div key={index}>{success.message}</div>
          ))}
        </div>
      );
    } else {
      return <React.Fragment></React.Fragment>;
    }
  }

  render() {
    return this.getSuccessAlert();
  }
}

// PropTypes
Error.propTypes = {
  success: PropTypes.array
};

const mapStateToProps = state => ({
  success: state.success.success
});

export default connect(
  mapStateToProps,
  {}
)(SuccessAlert);
