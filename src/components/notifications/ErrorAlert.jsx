import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class ErrorAlert extends Component {
  getErrorAlert() {
    const hasErrors = this.props.errors.length > 0;

    if (hasErrors) {
      return (
        <div className="alert alert-danger m-2">
          {this.props.errors.map((error, index) => (
            <div key={index}>{error.message}</div>
          ))}
        </div>
      );
    } else {
      return <React.Fragment></React.Fragment>;
    }
  }

  render() {
    return this.getErrorAlert();
  }
}

// PropTypes
Error.propTypes = {
  errors: PropTypes.array
};

const mapStateToProps = state => ({
  errors: state.errors.errors
});

export default connect(
  mapStateToProps,
  {}
)(ErrorAlert);
