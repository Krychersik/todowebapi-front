import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { UpdateTodoItemAction } from '../actions/UpdateTodoItemAction';
import { DeleteTodoItemAction } from '../actions/DeleteTodoItemAction';
import DateTimePicker from 'react-datetime-picker';

class EditTodoItem extends Component {
  state = {
    title: '',
    details: '',
    date: new Date(),
    subtasks: [],
    isShown: true
  };

  componentDidMount() {
    if (this.props.todo) {
      this.setState({
        title: this.props.todo.title,
        details: this.props.todo.details,
        date: this.props.todo.dateTime
          ? new Date(this.props.todo.dateTime)
          : new Date(),
        subtasks: this.props.todo.subtasks,
        isShown: true
      });
    }
  }

  onChange = e => this.setState({ [e.target.name]: e.target.value });

  onSubtaskChange = (e, index) => {
    const subtasks = [...this.state.subtasks];
    subtasks[index].title = e.target.value;

    this.setState({
      ...this.state,
      subtasks
    });
  };

  onDateTimeChange = date => this.setState({ ...this.state, date });

  onSubmit = e => {
    e.preventDefault();

    this.props.updateTodoItemAction({
      id: this.props.todo.id,
      title: this.state.title,
      details: this.state.details,
      dateTime: this.state.date.toJSON(),
      isComplete: this.props.todo.isComplete,
      subtasks: this.state.subtasks
    });
  };

  addSubtask = () => {
    const subtasks = [...this.state.subtasks, { title: '' }];
    this.setState({
      ...this.state,
      subtasks
    });
  };

  removeSubtask = index => {
    this.state.subtasks.splice(index, 1);

    this.setState({
      ...this.state
    });
  };

  showHideAddTodoItem() {
    this.setState({
      ...this.state,
      isShown: !this.state.isShown
    });
  }

  render() {
    return (
      <div className="alert alert-primary mt-2 mb-2">
        <div className={this.state.isShown ? 'mb-4' : 'mb-2'}>
          <b>Add Todo Item</b>

          <button
            className="btn btn-primary btn-sm float-right"
            type="button"
            onClick={() => {
              this.showHideAddTodoItem();
            }}
          >
            {this.state.isShown ? 'Hide' : 'Show'}
          </button>
        </div>

        {this.state.isShown && (
          <form onSubmit={this.onSubmit}>
            <div className="input-group mb-2">
              <div className="input-group-prepend">
                <span className="input-group-text">Title</span>
              </div>
              <input
                type="text"
                name="title"
                className="form-control"
                value={this.state.title}
                onChange={this.onChange}
              />
            </div>

            <div className="input-group mb-2">
              <div className="input-group-prepend">
                <span className="input-group-text">Details</span>
              </div>
              <input
                type="text"
                name="details"
                className="form-control"
                value={this.state.details}
                onChange={this.onChange}
              />
            </div>

            <div className="mb-2">
              <b>Date and time</b>
            </div>

            <DateTimePicker
              className="mb-2"
              onChange={this.onDateTimeChange}
              value={this.state.date}
            />

            <div>
              <b>Subtasks</b>
            </div>

            {this.state.subtasks.map((subtask, index) => (
              <div key={index}>
                <div className="input-group mb-2">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      Subtask {index + 1}
                    </span>
                  </div>
                  <input
                    type="text"
                    name="subtask"
                    className="form-control"
                    value={subtask.title}
                    onChange={e => {
                      this.onSubtaskChange(e, index);
                    }}
                  />

                  <button
                    className="btn btn-danger btn-sm ml-2"
                    type="button"
                    onClick={() => {
                      this.removeSubtask(index);
                    }}
                  >
                    Remove subtask
                  </button>
                </div>
              </div>
            ))}

            <button
              className="btn btn-primary btn-sm mt-2"
              type="button"
              onClick={() => {
                this.addSubtask();
              }}
            >
              Add subtask
            </button>

            <div
              className="mt-2"
              style={{ display: 'flex', marginLeft: 'auto' }}
            >
              <button
                type="button"
                className="btn btn-danger btn-sm mr-2 ml-auto"
                onClick={() => {
                  this.props.deleteTodoItemAction(this.props.todo.id);
                }}
              >
                Delete
              </button>

              <input
                className="btn btn-primary btn-sm"
                type="submit"
                value="Submit"
              />
            </div>
          </form>
        )}
      </div>
    );
  }
}

// PropTypes
EditTodoItem.propTypes = {
  updateTodoItemAction: PropTypes.func.isRequired,
  deleteTodoItemAction: PropTypes.func.isRequired,
  todo: PropTypes.object
};

export default connect(
  null,
  {
    updateTodoItemAction: UpdateTodoItemAction,
    deleteTodoItemAction: DeleteTodoItemAction
  }
)(EditTodoItem);
