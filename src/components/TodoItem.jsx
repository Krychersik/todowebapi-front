import React, { Component } from 'react';
import Moment from 'react-moment';
import MomentTimezone from 'moment-timezone';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { MarkTodoItemCompleteAction } from '../actions/MarkTodoItemCompleteAction';
import { MarkSubtaskCompleteAction } from '../actions/MarkSubtaskCompleteAction';

class TodoItem extends Component {
  getTextStyle = () => {
    return {
      fontWeight: 'bold',
      align: 'center',
      textDecoration: this.props.todo.isComplete ? 'line-through' : 'none'
    };
  };

  getSubtaskTextStyle = isComplete => {
    return {
      fontWeight: 'bold',
      align: 'center',
      textDecoration: isComplete ? 'line-through' : 'none'
    };
  };

  getCheckBox() {
    const { todo } = this.props;

    return (
      <div className="mr-auto custom-control custom-checkbox">
        <input
          className="custom-control-input"
          id={todo.id}
          type="checkbox"
          checked={todo.isComplete}
          onChange={() => {
            this.props.markTodoItemCompleteAction(todo.id, !todo.isComplete);
          }}
        />
        <label className="custom-control-label" htmlFor={todo.id}>
          <span style={this.getTextStyle()}>{todo.title}</span>
        </label>
      </div>
    );
  }

  getScheduledTime() {
    const { todo } = this.props;

    if (todo.dateTime) {
      var utcTime = MomentTimezone.utc(todo.dateTime).toDate();
      var localTime = MomentTimezone(utcTime).local();

      return (
        <div>
          Scheduled on:{' '}
          <b>
            <Moment date={localTime} format="DD MMM YYYY HH:mm" />
          </b>
        </div>
      );
    }
  }

  getSubtasks() {
    const { todo } = this.props;

    return todo.subtasks.map((subtask, index) => (
      <div key={index} style={{ marginLeft: '3rem' }}>
        {this.getSubtask(todo.id, index, subtask)}
      </div>
    ));
  }

  getSubtask(todoId, subtaskIndex, subtask) {
    return (
      <div className="mr-auto custom-control custom-checkbox">
        <input
          className="custom-control-input"
          id={subtask.id}
          type="checkbox"
          checked={subtask.isComplete}
          onChange={() => {
            this.props.markSubtaskCompleteAction(
              todoId,
              subtaskIndex,
              !subtask.isComplete
            );
          }}
        />
        <label className="custom-control-label" htmlFor={subtask.id}>
          <span style={this.getSubtaskTextStyle(subtask.isComplete)}>
            {subtask.title}
          </span>
        </label>
      </div>
    );
  }

  render() {
    const { todo } = this.props;

    return (
      <div className="p-3 list-group-item">
        <div className="d-flex">
          {this.getCheckBox()}
          {this.getScheduledTime()}
        </div>

        {todo.details.length > 0 && (
          <div className="ml-4">
            <b>Details:</b> {todo.details}
          </div>
        )}

        {todo.subtasks.length > 0 && (
          <div className="ml-4">
            <b>Subtasks</b>
          </div>
        )}

        {this.getSubtasks()}
      </div>
    );
  }
}

// PropTypes
TodoItem.propTypes = {
  todo: PropTypes.object.isRequired,
  markTodoItemCompleteAction: PropTypes.func.isRequired,
  markSubtaskCompleteAction: PropTypes.func.isRequired
};

export default connect(
  null,
  {
    markTodoItemCompleteAction: MarkTodoItemCompleteAction,
    markSubtaskCompleteAction: MarkSubtaskCompleteAction
  }
)(TodoItem);
