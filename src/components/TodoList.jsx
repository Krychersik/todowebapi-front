import React, { Component } from 'react';
import TodoItem from './TodoItem';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FetchTodoItemsAction } from '../actions/FetchTodoItemsAction';

class TodoList extends Component {
  componentDidMount() {
    this.props.fetchTodoItemsAction();
  }

  render() {
    return (
      <div>
        {this.props.todos.map(todo => (
          <TodoItem key={todo.id} todo={todo} />
        ))}
      </div>
    );
  }
}

// PropTypes
TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
  fetchTodoItemsAction: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  todos: state.todoItems.todos
});

export default connect(
  mapStateToProps,
  { fetchTodoItemsAction: FetchTodoItemsAction }
)(TodoList);
