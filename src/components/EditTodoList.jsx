import React, { Component } from 'react';
import EditTodoItem from './EditTodoItem';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FetchTodoItemsAction } from '../actions/FetchTodoItemsAction';

class EditTodoList extends Component {
  componentDidMount() {
    this.props.fetchTodoItemsAction();
  }

  render() {
    return (
      <div>
        {this.props.todos.map(todo => (
          <EditTodoItem key={todo.id} todo={todo} />
        ))}
      </div>
    );
  }
}

// PropTypes
EditTodoList.propTypes = {
  todos: PropTypes.array.isRequired,
  fetchTodoItemsAction: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  todos: state.todoItems.todos
});

export default connect(
  mapStateToProps,
  { fetchTodoItemsAction: FetchTodoItemsAction }
)(EditTodoList);
