import {
  FETCH_TODO_ITEMS,
  ADD_TODO_ITEM,
  DELETE_TODO_ITEM,
  UPDATE_TODO_ITEM,
  MARK_TODO_ITEM_COMPLETE,
  MARK_SUBTASK_COMPLETE
} from '../actions/Types';

const initialState = {
  todos: []
};

const TodoItemsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TODO_ITEMS:
      return {
        ...state,
        todos: action.payload
      };

    case ADD_TODO_ITEM:
      return {
        ...state,
        todos: [...state.todos, action.payload]
      };

    case DELETE_TODO_ITEM:
      return {
        ...state,
        todos: state.todos.filter(t => t.id !== action.payload)
      };

    case UPDATE_TODO_ITEM:
      return {
        ...state,
        todos: state.todos.map(t => {
          if (t.id === action.payload.todoItem.id) {
            t = {
              ...action.payload.todoItem
            };
          }

          return t;
        })
      };

    case MARK_TODO_ITEM_COMPLETE:
      return {
        ...state,
        todos: state.todos.map(t => {
          if (t.id === action.payload.id) {
            t = {
              ...t,
              isComplete: action.payload.value
            };
          }

          return t;
        })
      };

    case MARK_SUBTASK_COMPLETE:
      return {
        ...state,
        todos: state.todos.map(t => {
          if (t.id === action.payload.taskId) {
            const subtask = t.subtasks[action.payload.subtaskIndex];

            t.subtasks[action.payload.subtaskIndex] = {
              ...subtask,
              isComplete: action.payload.value
            };

            t = {
              ...t
            };
          }

          return t;
        })
      };

    default:
      return state;
  }
};

export default TodoItemsReducer;
