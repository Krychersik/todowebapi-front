import { combineReducers } from 'redux';
import TodoItemsReducer from './TodoItemsReducer';
import ErrorsReducer from './ErrorsReducer';
import SuccessReducer from './SuccessReducer';

const Reducers = combineReducers({
  todoItems: TodoItemsReducer,
  errors: ErrorsReducer,
  success: SuccessReducer
});

export default Reducers;
