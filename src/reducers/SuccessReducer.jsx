import {
  ADD_TODO_ITEM,
  DELETE_TODO_ITEM,
  UPDATE_TODO_ITEM
} from '../actions/Types';

const initialState = {
  success: []
};

const SuccessReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO_ITEM:
      return {
        ...state,
        success: [
          {
            message: 'Todo Item was successfully added to the list.'
          }
        ]
      };

    case DELETE_TODO_ITEM:
      return {
        ...state,
        success: [
          {
            message: 'Todo Item was successfully deleted.'
          }
        ]
      };

    case UPDATE_TODO_ITEM:
      return {
        ...state,
        success: [
          {
            message: 'Todo Item was successfully updated.'
          }
        ]
      };

    default:
      return {
        success: []
      };
  }
};

export default SuccessReducer;
