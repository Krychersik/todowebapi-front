import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import Header from './layouts/Header';
import AboutPage from './components/pages/AboutPage';
import AddTodoItemPage from './components/pages/AddTodoItemPage';
import EditTodoListPage from './components/pages/EditTodoListPage';
import ErrorAlert from './components/notifications/ErrorAlert';
import SuccessAlert from './components/notifications/SuccessAlert';
import TodoListPage from './components/pages/TodoListPage';

class App extends Component {
  render() {
    return (
      <Router>
        <React.Fragment>
          <main className="container">
            <Header />
            <ErrorAlert />
            <SuccessAlert />

            <Route
              exact
              path="/"
              render={props => (
                <React.Fragment>
                  <TodoListPage />
                </React.Fragment>
              )}
            />

            <Route
              path="/addTodoItem"
              render={props => (
                <React.Fragment>
                  <AddTodoItemPage />
                </React.Fragment>
              )}
            />

            <Route
              path="/editTodoList"
              render={props => (
                <React.Fragment>
                  <EditTodoListPage />
                </React.Fragment>
              )}
            />

            <Route
              path="/about"
              render={props => (
                <React.Fragment>
                  <AboutPage />
                </React.Fragment>
              )}
            />
          </main>
        </React.Fragment>
      </Router>
    );
  }
}

export default App;
