import axios from 'axios';
import { ADD_TODO_ITEM, ERROR } from './Types';

export const AddTodoItemAction = todoItem => dispatch => {
  axios
    .post('https://localhost:5001/api/v1/todo', {
      title: todoItem.title,
      details: todoItem.details,
      dateTime: todoItem.dateTime,
      isComplete: todoItem.isComplete,
      subtasks: todoItem.subtasks
    })
    .then(response => {
      dispatch({
        type: ADD_TODO_ITEM,
        payload: response.data.data
      });
    })
    .catch(error => {
      if (error.response.data.errors) {
        dispatch({
          type: ERROR,
          payload: error.response.data.errors
        });
      } else {
        dispatch({
          type: ERROR,
          payload: [
            {
              title: error.response.data,
              message: error.response.data
            }
          ]
        });
      }
    });
};
