import axios from 'axios';
import { FETCH_TODO_ITEMS, ERROR } from './Types';

export const FetchTodoItemsAction = () => dispatch => {
  axios
    .get('https://localhost:5001/api/v1/todo')
    .then(response => {
      dispatch({
        type: FETCH_TODO_ITEMS,
        payload: response.data.data
      });
    })
    .catch(error => {
      dispatch({
        type: ERROR,
        payload: [
          {
            title: 'Data loading problem',
            message: "Couldn't load the data"
          }
        ]
      });
    });
};
