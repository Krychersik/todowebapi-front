import axios from 'axios';
import { MARK_SUBTASK_COMPLETE, ERROR } from './Types';

export const MarkSubtaskCompleteAction = (
  taskId,
  subtaskIndex,
  value
) => dispatch => {
  axios
    .patch(`https://localhost:5001/api/v1/todo/${taskId}`, [
      {
        path: `/subtasks/${subtaskIndex}/isComplete`,
        op: 'replace',
        value: value
      }
    ])
    .then(response => {
      dispatch({
        type: MARK_SUBTASK_COMPLETE,
        payload: { taskId, subtaskIndex, value }
      });
    })
    .catch(error => {
      if (error.response.data.errors) {
        dispatch({
          type: ERROR,
          payload: error.response.data.errors
        });
      } else {
        dispatch({
          type: ERROR,
          payload: [
            {
              title: error.response.data,
              message: error.response.data
            }
          ]
        });
      }
    });
};
