import axios from 'axios';
import { UPDATE_TODO_ITEM, ERROR } from './Types';

export const UpdateTodoItemAction = todoItem => dispatch => {
  axios
    .put(`https://localhost:5001/api/v1/todo/${todoItem.id}`, {
      title: todoItem.title,
      details: todoItem.details,
      dateTime: todoItem.dateTime,
      isComplete: todoItem.isComplete,
      subtasks: todoItem.subtasks
    })
    .then(response => {
      dispatch({
        type: UPDATE_TODO_ITEM,
        payload: { todoItem }
      });
    })
    .catch(error => {
      if (error.response.data.errors) {
        dispatch({
          type: ERROR,
          payload: error.response.data.errors
        });
      } else {
        dispatch({
          type: ERROR,
          payload: [
            {
              title: error.response.data,
              message: error.response.data
            }
          ]
        });
      }
    });
};
