import axios from 'axios';
import { DELETE_TODO_ITEM, ERROR } from './Types';

export const DeleteTodoItemAction = id => dispatch => {
  axios
    .delete(`https://localhost:5001/api/v1/todo/${id}`)
    .then(response => {
      dispatch({
        type: DELETE_TODO_ITEM,
        payload: id
      });
    })
    .catch(error => {
      if (error.response.data.errors) {
        dispatch({
          type: ERROR,
          payload: error.response.data.errors
        });
      } else {
        dispatch({
          type: ERROR,
          payload: [
            {
              title: error.response.data,
              message: error.response.data
            }
          ]
        });
      }
    });
};
