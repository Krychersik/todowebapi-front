import axios from 'axios';
import { MARK_TODO_ITEM_COMPLETE, ERROR } from './Types';

export const MarkTodoItemCompleteAction = (id, value) => dispatch => {
  axios
    .patch(`https://localhost:5001/api/v1/todo/${id}`, [
      {
        path: '/isComplete',
        op: 'replace',
        value: value
      }
    ])
    .then(response => {
      dispatch({
        type: MARK_TODO_ITEM_COMPLETE,
        payload: { id, value }
      });
    })
    .catch(error => {
      if (error.response.data.errors) {
        dispatch({
          type: ERROR,
          payload: error.response.data.errors
        });
      } else {
        dispatch({
          type: ERROR,
          payload: [
            {
              title: error.response.data,
              message: error.response.data
            }
          ]
        });
      }
    });
};
